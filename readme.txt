Construction application :

    docker-compose up --build


Connexion et visualisation terminal postgres :

    docker exec -it postgres_container psql -U root db_postgres
    SELECT * FROM Client;


Connexion et visualisation terminal mongodb :

    docker exec -it mongodb_container bash
    mongosh -u root -p root --authenticationDatabase admin
    use db_mongodb
    db.clients.find().pretty()
