<?php
require 'vendor/autoload.php';

// Vérification des données du formulaire
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['date_naissance'])) {
    $nom = trim($_POST['nom']);
    $prenom = trim($_POST['prenom']);
    $date_naissance = trim($_POST['date_naissance']);

    // Vérification que les champs ne sont pas vides
    if (!empty($nom) && !empty($prenom) && !empty($date_naissance)) {
        // Connexion à la base de données PostgreSQL
        $host = "postgresql";
        $port = "5432";
        $dbname = "db_postgres";
        $user = "root";
        $password = "root";

        // Chaîne de connexion à la base de données
        $connection_string = "host={$host} port={$port} dbname={$dbname} user={$user} password={$password}";
        $dbconn = pg_connect($connection_string);

        if (!$dbconn) {
            die("Erreur de connexion à la base PostgreSQL.");
        }

        // Nettoyage et préparation des données
        $nom = pg_escape_string($dbconn, $nom);
        $prenom = pg_escape_string($dbconn, $prenom);
        $date_naissance = pg_escape_string($dbconn, $date_naissance);

        // Préparation de la requête SQL pour insérer les données dans PostgreSQL
        $query = "INSERT INTO client (ClientName, ClientFirstName, ClientDateBirth) VALUES ('$nom', '$prenom', '$date_naissance')";

        // Exécution de la requête PostgreSQL
        $result = pg_query($dbconn, $query);

        // Connexion à MongoDB
        try {
            $mongoClient = new MongoDB\Client("mongodb://root:root@mongodb_container:27017");
            $mongoDb = $mongoClient->selectDatabase('db_mongodb');
            $mongoCollection = $mongoDb->selectCollection('clients');

            // Insertion dans MongoDB
            $insertResult = $mongoCollection->insertOne([
                'nom' => $nom,
                'prenom' => $prenom,
                'date_naissance' => $date_naissance
            ]);
        } catch (MongoDB\Driver\Exception\Exception $e) {
            // Gestion des erreurs MongoDB
        }

        header('Location: index.php');
        exit();
    }
}

// Si des champs sont vides ou si la méthode n'est pas POST, rediriger vers index.php
header('Location: index.php');
exit();
?>
