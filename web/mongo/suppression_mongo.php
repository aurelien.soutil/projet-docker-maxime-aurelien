<?php
require '../vendor/autoload.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Récupération de l'ID client à supprimer
    if(isset($_POST['client_id'])) {
        $client_id = $_POST['client_id'];

        // Connexion à MongoDB
        $mongoClient = new MongoDB\Client("mongodb://root:root@mongodb_container:27017");
        $mongoDb = $mongoClient->selectDatabase('db_mongodb');
        $mongoCollection = $mongoDb->selectCollection('clients');

        // Vérifier si l'ID a la structure d'un ObjectId
        if (!preg_match('/^[a-f\d]{24}$/i', $client_id)) {
            exit;
        }

        // Suppression du document en utilisant l'ID du client
        $deleteResult = $mongoCollection->deleteOne(['_id' => new MongoDB\BSON\ObjectId($client_id)]);

    }
}


header('Location: ../index.php');
exit();

?>
