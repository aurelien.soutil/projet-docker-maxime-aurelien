<?php
require 'vendor/autoload.php';

try {
    $mongoClient = new MongoDB\Client("mongodb://root:root@mongodb_container:27017");
    $mongoDb = $mongoClient->selectDatabase('db_mongodb');
    $mongoCollection = $mongoDb->selectCollection('clients');

    // Récupérer les données clients depuis MongoDB
    $mongoData = $mongoCollection->find();

    foreach ($mongoData as $client) {
        echo "<tr>";
        echo "<td>" . $client['nom'] . "</td>";
        echo "<td>" . $client['prenom'] . "</td>";
        echo "<td>" . $client['date_naissance'] . "</td>";
        echo "<td><form action='/mongo/suppression_mongo.php' method='post'>
                    <input type='hidden' name='client_id' value='" . $client['_id'] . "'>
                    <input type='submit' value='Supprimer'>
                </form></td>";
        echo "</tr>";
    }
} catch (MongoDB\Driver\Exception\Exception $e) {
    echo "Erreur lors de la récupération des données MongoDB: " . $e->getMessage();
}
?>