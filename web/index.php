<!DOCTYPE html>
<html>
<head>
<style>
  body {
    font-family: Arial, sans-serif;
    background-color: #f4f4f4;
    margin: 0;
    padding: 20px;
  }

  h2 {
    text-align: center;
    color: #333;
  }

  .container {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .form-container {
    max-width: 400px;
    margin-bottom: 20px;
    background: #fff;
    padding: 20px;
    border-radius: 8px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
  }

  label {
    display: block;
    margin-bottom: 5px;
    color: #333;
  }

  input[type="text"],
  input[type="date"],
  input[type="submit"] {
    width: calc(100% - 20px);
    padding: 8px;
    margin-bottom: 15px;
    border: 1px solid #ccc;
    border-radius: 4px;
  }

  input[type="submit"] {
    width: 100%;
    background-color: #4caf50;
    color: white;
    cursor: pointer;
  }

  input[type="submit"]:hover {
    background-color: #45a049;
  }

  .tables-container {
    display: flex;
    justify-content: space-between;
    width: calc(100% - 40px);
  }

  table {
    width: 48%;
    border-collapse: collapse;
    background-color: #fff;
    border-radius: 8px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
  }

  table caption {
    font-weight: bold;
    margin-bottom: 10px;
    text-align: left;
    font-size: 1.2em;
    padding: 8px;
    background-color: #4caf50;
    color: white;
    border-radius: 8px 8px 0 0;
  }

  table th,
  table td {
    padding: 8px;
    text-align: center;
    border-bottom: 1px solid #ddd;
  }

  table th {
    background-color: #f2f2f2;
    font-weight: normal;
  }
</style>
<title>Formulaire Client</title>
</head>
<body>

<h2>Formulaire Client</h2>

<div class="container">

  <div class="form-container">
    <form action="insertion.php" method="post">
      <label for="nom">Nom :</label>
      <input type="text" id="nom" name="nom">
      <label for="prenom">Prénom :</label>
      <input type="text" id="prenom" name="prenom">
      <label for="date_naissance">Date de naissance :</label>
      <input type="date" id="date_naissance" name="date_naissance">
      <input type="submit" value="Envoyer">
    </form>
  </div>

  <div class="tables-container">
    <table border="1">
      <caption>Liste des Clients Postgres</caption>
      <thead>
        <tr>
          <th>Nom</th>
          <th>Prénom</th>
          <th>Date de Naissance</th>
          <th>Suppression</th>
        </tr>
      </thead>
      <tbody>
        <?php include 'postgres/affichage_postgres.php'; ?>
      </tbody>
    </table>

    <table border="1">
      <caption>Liste des Clients MongoDB</caption>
      <thead>
        <tr>
          <th>Nom</th>
          <th>Prénom</th>
          <th>Date de Naissance</th>
          <th>Suppression</th>
        </tr>
      </thead>
      <tbody>
        <?php include 'mongo/affichage_mongo.php'; ?>
      </tbody>
    </table>
  </div>

</div>

</body>
</html>