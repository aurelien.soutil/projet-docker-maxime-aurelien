<?php
// Connexion à la base de données
$host = "postgresql";
$port = "5432";
$dbname = "db_postgres";
$user = "root";
$password = "root";

$connection_string = "host={$host} port={$port} dbname={$dbname} user={$user} password={$password}";
$dbconn = pg_connect($connection_string);

if (!$dbconn) {
    die("Erreur de connexion à la base de données PostgreSQL");
}

// Récupération des données des clients depuis la table 'client'
$query = "SELECT * FROM client";
$result = pg_query($dbconn, $query);

if ($result) {
    
    while ($row = pg_fetch_assoc($result)) {
        echo "<tr>";
        echo "<td>" . $row['clientname'] . "</td>";
        echo "<td>" . $row['clientfirstname'] . "</td>";
        echo "<td>" . $row['clientdatebirth'] . "</td>";

        
        echo "<td>";
        echo "<form action='/postgres/suppression_postgres.php' method='post'>";
        echo "<input type='hidden' name='client_id' value='" . $row['clientid'] . "'>";
        echo "<input type='submit' value='Supprimer'>";
        echo "</form>";
        echo "</td>";

        echo "</tr>";
    }
    
    pg_free_result($result);
} else {
    echo "Erreur lors de la récupération des données des clients";
}

// Fermeture de la connexion
pg_close($dbconn);
?>