<?php
// Vérifie si un ID client a été soumis
if (isset($_POST['client_id'])) {
    // Récupère l'ID du client depuis le formulaire
    $client_id = $_POST['client_id'];

    // Connexion à la base de données
    $host = "postgresql";
    $port = "5432";
    $dbname = "db_postgres";
    $user = "root";
    $password = "root";

    $connection_string = "host={$host} port={$port} dbname={$dbname} user={$user} password={$password}";
    $dbconn = pg_connect($connection_string);

    if (!$dbconn) {
        die("Erreur de connexion à la base de données PostgreSQL");
    }

    $query = "DELETE FROM client WHERE clientid = $client_id";

    // Exécution de la requête de suppression
    $result = pg_query($dbconn, $query);

    if ($result) {
        // Fermeture de la connexion à la base de données
        pg_close($dbconn);
        header('Location: ../index.php');
        exit();
    } else {
        echo "Erreur lors de la suppression du client";
    }

} else {
    echo "Aucun ID de client spécifié pour la suppression";
}
?>