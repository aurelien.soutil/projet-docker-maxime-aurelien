# Utilisez l'image PHP officielle avec Apache
FROM php:apache

# Définissez l'environnement COMPOSER_ALLOW_SUPERUSER pour permettre à Composer de s'exécuter en tant qu'utilisateur root dans le conteneur
RUN export COMPOSER_ALLOW_SUPERUSER=1

# Installez les paquets nécessaires pour l'extension zip et les utilitaires d'extraction
RUN apt-get update \
    && apt-get install -y libpq-dev zip unzip \
    && docker-php-ext-install pdo pdo_pgsql pgsql \
    && pecl install mongodb \
    && docker-php-ext-enable mongodb

# Copiez les fichiers de votre application dans le conteneur
COPY ./web /var/www/html

# Définissez le répertoire de travail
WORKDIR /var/www/html

# Installez Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Exécutez Composer pour installer les dépendances, y compris la bibliothèque MongoDB
RUN composer require mongodb/mongodb

# Indiquez les ports à exposer
EXPOSE 80