CREATE TABLE Client (
    ClientId SERIAL PRIMARY KEY,
    ClientName VARCHAR(100),
    ClientFirstName VARCHAR(100),
    ClientDateBirth DATE
);
